@isTest
public with sharing class CountControllerTest {
    @testSetup
    public static void setup() {
        Account testAccount = new Account (
            Name = 'Test Account'
        );
        insert testAccount;  

        Contact jackRecord = new Contact (
            FirstName = 'Jack',
            LastName = 'Reacher',
            AccountId = testAccount.Id,
            Sector__c = 'Institutional Investment',
            Sector_Details__c = 'Insurance'
        );
        insert jackRecord;

        Gold_org_Download__c downloadRecord = new Gold_org_Download__c (
            Name = 'Download record',
            Download_URL__c = '/testurl',
            Download_Url_Index__c = '/test'
        );
        insert downloadRecord;

        Gold_org_Pageview__c pageviewRecord = new Gold_org_Pageview__c (
            Name = 'Pageview record'
        );
        insert pageviewRecord;

        Gold_org_User_Download__c userFirstDownload = new Gold_org_User_Download__c (
            Contact__c = jackRecord.Id,
            Gold_org_Download__c = downloadRecord.Id
        );
        insert userFirstDownload;

        Gold_org_User_Download__c userSecondDownload = new Gold_org_User_Download__c (
            Contact__c = jackRecord.Id,
            Gold_org_Download__c = downloadRecord.Id
        );
        insert userSecondDownload;
        
        Gold_org_User_Pageview__c userFirstpageview = new Gold_org_User_Pageview__c (
            Contact__c = jackRecord.Id,
            Gold_org_Pageview__c = pageviewRecord.Id
        );
        insert userFirstpageview;
    }

    @isTest static void testFindContactsInformation_GetAllFields() {
        Account testAccount = [SELECT Id FROM Account WHERE Name = 'Test Account' WITH SECURITY_ENFORCED LIMIT 1];
        List<CountController.ContactInfo> contactsInfo = CountController.findContactsInformation(testAccount.Id);
        System.assert(contactsInfo.size() == 1);
        System.assert(contactsInfo.get(0).contactName == 'Jack Reacher');
        System.assert(contactsInfo.get(0).downloads == 2);
        System.assert(contactsInfo.get(0).pagewiews == 1);
    }
}
