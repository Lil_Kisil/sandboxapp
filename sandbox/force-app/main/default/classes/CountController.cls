public with sharing class CountController {
    @AuraEnabled(cacheable=true)
    public static List<ContactInfo> findContactsInformation(Id recordId){
    List<Account> accountInfo = [SELECT (SELECT Id, Name FROM Contacts) FROM Account WHERE Id =:recordId WITH SECURITY_ENFORCED];
    List<Contact> contactsData = accountInfo[0].Contacts;
    
    List<ContactInfo> listInfoFromCotacts = new List<ContactInfo>();

    for(Integer i = 0; i < contactsData.size(); i++){
        Integer downloads = [SELECT COUNT() FROM Gold_org_User_Download__c WHERE Contact__c =:contactsData.get(i).Id WITH SECURITY_ENFORCED];
        Integer pagewiews = [SELECT COUNT() FROM Gold_org_User_Pageview__c WHERE Contact__c =:contactsData.get(i).Id WITH SECURITY_ENFORCED];
        listInfoFromCotacts.add(new ContactInfo(contactsData.get(i).Id, contactsData.get(i).Name, downloads, pagewiews));
    }

    return listInfoFromCotacts;
    }


    public class ContactInfo {
        @AuraEnabled
        public String contactId;
        @AuraEnabled
        public String contactName;
        @AuraEnabled
        public Integer downloads;
        @AuraEnabled
        public Integer pagewiews;

        public ContactInfo(String contactId, String contactName, Integer downloads, Integer pagewiews) {
            this.contactId = contactId;
            this.contactName = contactName;
            this.downloads = downloads;
            this.pagewiews = pagewiews;
        }
    }
}