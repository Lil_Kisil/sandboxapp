import { LightningElement, api, track, wire } from 'lwc';
import findContactsInformation from '@salesforce/apex/CountController.findContactsInformation';

export default class ListOfContacts extends LightningElement {
    @api recordId;
    @track errorMsg;
                
    @wire(findContactsInformation, { 
        recordId: '$recordId'
    })
    contactRecords

}