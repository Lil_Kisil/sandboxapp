import { LightningElement, api } from 'lwc';

export default class ContactItem extends LightningElement {
    @api record;

    get checkIsFieldExist() {
        if (this.record.downloads != 0 || this.record.pagewiews != 0) {
                return true;
            } else {
                return false;
            }
    }

    get getUrlPathToContact(){
        return '/lightning/r/Contact/' + this.record.contactId + '/view';
    }
}